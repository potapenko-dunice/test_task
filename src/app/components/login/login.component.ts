import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Router} from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  constructor(private authenticationService: AuthenticationService,
    private router: Router) { }

  ngOnInit() {
    this.authenticationService.logout();
  }

  login() {
    this.authenticationService
      .login(this.username, this.password)
      .subscribe((res) => {
        this.router.navigate(['/']);
      }, (err) => {
        console.log(err);
      });
  }

}
