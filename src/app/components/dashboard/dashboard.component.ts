import { Component, OnInit } from '@angular/core';
import { DashboardService} from '../../services/dashboard.service';
import { Router } from '@angular/router';
import { AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  tasklist: Array<string>
  constructor(private dashboardService: DashboardService,
              private router: Router,
              private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.tasklist= [];
    this.dashboardService.getTask().subscribe((res)=>{this.tasklist=res[0].task})
  }
  logOut(){
    this.authenticationService.logout();
    this.router.navigate(['/token'])
  }
}

class task {
  message: string;
  check: boolean;
}