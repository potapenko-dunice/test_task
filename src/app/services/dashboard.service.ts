import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { AuthenticationService } from './authentication.service'
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
@Injectable()
export class DashboardService {

  constructor(private http: Http,
    private authenticationService: AuthenticationService) { }
  getTask() {
    let token = this.authenticationService.token;
    let headers = new Headers({ 'Authorization': 'JWT ' + this.authenticationService.token });
    let requestOptions = new RequestOptions({ headers: headers })
    return this.http.get('/api/dashboard', requestOptions)
      .map((response) => {
        return response.json();
      });
  }
}
