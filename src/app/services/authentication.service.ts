import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
@Injectable()
export class AuthenticationService {
  public token: string;
  
  constructor(private http: Http) {
    var userToken = JSON.parse(localStorage.getItem('currentUser'));
    this.token = userToken && userToken.token;
  }

  login(username: string, password: string) {
    return this.http.post('/api/authenticate', JSON.stringify({ username: username, password: password }))
      .map((response) => {
        let token = response.json() && response.json().token;
        if (token) {

          this.token = token;

          // store username and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
          // return true to indicate successful login
          return response.json();
        } else {
          // return false to indicate failed login
          return response.json();
        }
      }).catch((err) => { return Observable.throw(err) });
  }

  logout() {
    this.token = null;
    localStorage.removeItem('currentUser');
  }
}
