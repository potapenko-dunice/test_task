import { Directive, Input, TemplateRef, ViewContainerRef, Renderer, ElementRef } from '@angular/core';

@Directive({
  selector: '[task]'
})
export class TaskDirective  {
  @Input('task') id;

  listener: any;

  constructor(private templateRef:TemplateRef<any>,
              private elementRef: ElementRef,
              private viewContainerRef:ViewContainerRef,
              private renderer: Renderer) {
              };
  ngOnInit() {
   
    this.viewContainerRef.createEmbeddedView(this.templateRef);
    
    setTimeout(()=>{
      let input = this.renderer.selectRootElement(`#input-${this.id}`);
      this.listener = this.renderer.listen(input, 'change',(event)=>{
        this.viewContainerRef.clear()
      })
    })
    
  }
  ngOnDestroy() {
    if(this.listener) {
      this.listener();
    }
  }
}
