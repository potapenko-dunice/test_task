import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import {MOCK_USER} from './mock-user';
import {TASK} from './task'
let testUser = MOCK_USER;
let task = TASK;
export function fakeBackendFactory(backend: MockBackend, options: BaseRequestOptions) {
    // configure fake backend
    backend.connections.subscribe((connection: MockConnection) => {
        
        setTimeout(() => {
 
            // fake authenticate api end point
            if (connection.request.url.endsWith('/api/authenticate') && connection.request.method === RequestMethod.Post) {
                
                // get parameters from post request
                let params = JSON.parse(connection.request.getBody());
                if(!params.username || !params.password){
                    return connection.mockRespond(new Response( new ResponseOptions({status:401, body: {message: 'Uncorrect input'}})))
                }
                // check user credentials and return fake jwt token if valid
                let password;
                for(let test of testUser){
                    if(test.username==params.username){
                        password=test.password
                    }
                }
                if (params.password === password) {
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200, body: { token: 'fake-jwt-token' } })
                    ));
                } else {
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200 })
                    ));
                }
            }

            if (connection.request.url.endsWith('/api/dashboard') && connection.request.method === RequestMethod.Get) {
                if (connection.request.headers.get('Authorization') === 'JWT fake-jwt-token'){
                    connection.mockRespond(new Response(
                        new ResponseOptions({ status: 200, body: [{task:task}] })
                    ));
                }
                
            }
        }, 500);
 
    });
 
    return new Http(backend, options);
}
 
export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: Http,
    useFactory: fakeBackendFactory,
    deps: [MockBackend, BaseRequestOptions]
};
