# TaskService

This project was generated with [angular-cli](https://github.com/angular/angular-cli).

## angular-cli
Pls, install the last version of angular-cli

## Install
`
yarn install
`
OR
`
npm install
`

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Test credentials
`username: test`
`password: test`